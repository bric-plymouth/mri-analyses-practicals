.. _SPM_intro:

====================
Introduction to SPM
====================

.. figure:: img/spm12_logo.png
   :width: 200px
   :align: right

What is SPM?
--------------------

`SPM <https://www.fil.ion.ucl.ac.uk/spm/>`__ (Statistical Parametric Mapping) is an fMRI analysis software package that runs in `Matlab <https://www.mathworks.com/products/matlab.html>`__. In addition to fMRI analysis, SPM contains toolboxes for performing volume-based morphometry and effective connectivity.
If you need to install SPM on you personal computer, the `SPM website <https://www.fil.ion.ucl.ac.uk/spm/software/spm12/>`__ has instructions on how to install the software package. 

Start Matlab
--------------------

Matlab can be started using its icon at the bottom of the screen.

Start SPM
--------------------

The SPM package is already installed on the server, in /usr/local/spm/.
To start it, matlab needs to know where to find smp and store its path. This should already be done on the server but if spm does start, you will have to do it. Open up Matlab, click on the "Home" tab, and then click the "Set Path" button. Select the ``spm12`` directory, and then click "Add Folder". Click the "Save" button to ensure that the path is set every time Matlab is opened, and then close the window.

.. figure:: img/SPM_SetPath.png

After you have set the path, type the following from the Matlab terminal:

::

  spm
  
Which will open up the following window:

.. figure:: img/Type_SPM.png

Click on the fMRI button to open the SPM fMRI GUI.

.. note::

  Assuming that you are just using the fMRI part of the SPM package, you can type ``spm fmri`` from the command line to open up the fMRI analysis GUI.

SPM Interface
--------------------
  
.. figure:: img/SPM_interface.png

.. note::

  SPM can read any image that are in NIFTI format, but they cannot be compressed - that is, if the datasets end with a ``.gz`` extension, you will first need to unzip them by navigating to the directory containing the images and then type in the matlab terminal:

  ::

    gunzip('*.gz')
    
  Which will expand the images and remove the ``.gz`` extension.


You are now ready to explore the dataset.