========================
Resources
========================

ONLINE
--------------------

`Principles of fMRI <https://www.coursera.org/learn/functional-mri>`__  Brilliant video-based online course developed by Martin Lindquist and Tor Wager at Johns Hopkins University.

`NeWBI4fMRI <https://www.newbi4fmri.com>`__  is a course developed by Jody Culham at Western University. Based on BrainVoyager software

`Andys Brain Book <https://andys-brain-book.readthedocs.io>`__ is a course developed by Andy Jahn at University of Michigan. Mainly based on FSL software. Includes beginner material as well as more advanced tutorial on Unix, FreeSurfer, AFNI, CONN toolbox (Functional connectivity), FIR models, ASL, Machine learning.

`MRC Cognition and Brain Sciences Unit WIKI <https://imaging.mrc-cbu.cam.ac.uk/imaging/CbuImaging>`__

`Chris Rordens Neuropsychology Lab Documentation <https://crnl.readthedocs.io>`__ 

`SPM course and email list <https://www.fil.ion.ucl.ac.uk/spm>`__ 

`INCF Neurostars <https://neurostars.org>`__ A question and answer forum for neuroscience researchers


RECOMMENDED BOOKS
--------------------

*Functional Magnetic Resonance Imaging*, by Huettel, Song, & McCarthy (3rd Edition). 


MRI physics
--------------------

chapters 1-5 of the book *Functional Magnetic Resonance Imaging*, by Huettel, Song, & McCarthy (3rd Edition). 
Allen Elster's `MRI Questions <http://mriquestions.com/index.html>`__ website for useful illustrations of MRI concepts.