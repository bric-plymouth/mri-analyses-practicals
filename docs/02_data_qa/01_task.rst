========================
The task
========================

Subjects performed 5 runs, each containing 20 faces, 20 objects and 15 butterfly trials, presented in a pseudorandom order. Participants had to press a button when they see a butterfly.

Functional imaging data were acquired using a research dedicated Siemens Prisma 3.0 T scanner, with a standard Siemens 32 channels head coil, located at the Brain Research and Imaging Centre (BRIC) in Plymouth. 

Conditions
--------------------

.. figure:: 01_task.png

Timing
--------------------

Trials were presented in a pseudorandom order, with a intertrial interval (ITI) of 1.5, 3 or 4.5s.

.. figure:: 01_task_timing.png


Research questions
--------------------

- Which brain areas are more activated when seeing a face rather than an object?
- Which brain areas are more activated when seeing an object rather than a face?
- Which brain areas are activated when pressing the button (finger movement)?
