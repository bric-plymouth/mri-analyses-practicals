.. _Preprocessing:

==========================
Preprocessing overview
==========================
   
Overview
-------------

Now that we know where our data is and what it looks like, we will do the first step of fMRI analysis: **Preprocessing**.


.. Important::
  Preprocessing data will apply some changes on them. It is important that you work on your local copy of the data. This way you can always make another copy if you need to start over.  We will work in directory ``/home/lab1user/STUDENT-DATA/PSYC724-LAB-DATA/03_derivatives/spm-preproc/``. If you do not have you local copy of the data there, you copy the data from ``/home/lab1user/SHARED_labdata/PSYC724-LAB-DATA``

Think of preprocessing as cleaning up the images in order to increase the signal-to-noise ratio.
When we preprocess fMRI data we are cleaning up the three-dimensional images that we acquire every :ref:`TR <Repetition_Time>`. An fMRI volume contains not only the signal that we are interested in - changes in oxygenated blood - but also fluctuations that we are not interested in, such as head motion, random drifts, breathing, and heartbeats. We call these other fluctuations **noise**, since we want to separate them from the signal that we are interested in. Some of these can be regressed out of the data by modeling them (which is discussed in the section on modeling fitting), and others can be reduced or removed by preprocessing.

To begin preprocessing data, read through the following section. We will begin with **Slice-Timing Correction**, which correct misalignments and timing errors in the functional images, then **Realignment**, before moving on to **Coregistration** and **Normalization**, which align the functional and structural images and move them both to a standardized space. Finally, the images are **Smoothed** in order to increase signal and cancel out noise. The typical sequence of preprocessing steps is numbered in the image below:

.. figure:: 00_SPM_GUI_Steps.png

.. note::
  These steps can be performed one after the other, or the whole sequence can be planed in a batch job. 


.. note::
  Different software packages will do these steps in slightly different order - for example, FSL will normalize the statistical maps after model fitting. There are also analyses which omit certain steps - for example, some people who do multi-voxel pattern analyses don't smooth their data. In any case, the list above represents the most common steps that are performed on a typical dataset.

---------

Video
---------

Once you have finished reviewing all of the preprocessing steps, click `here <https://www.youtube.com/watch?v=zSqBoB1GrDk>`__ for a video showing how to do all of the steps after realignment.