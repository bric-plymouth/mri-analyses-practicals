.. _01_SliceTiming:

==================================
Slice-Timing Correction
==================================


Background
***********

Unlike a photograph, in which the entire picture is taken in a single moment, an fMRI volume is acquired in slices. Each of these slices takes time to acquire - from tens to hundreds of milliseconds.

The two most commonly used methods for creating volumes are sequential and interleaved slice acquisition. Sequential slice acquisition acquires each adjacent slice consecutively, either bottom-to-top or top-to-bottom. Interleaved slice acquisition acquires every other slice, and then fills in the gaps on the second pass. Both of these methods are illustrated in the video below.

.. figure:: 01_SliceTimingCorrection_Demo.gif


.. note::

  For another illustration of how slice-timing works using **linear interpolation**, see `Matthew Brett's page <https://matthew-brett.github.io/teaching/slice_timing.html>`__.

As you'll see later on, when we model the data at each voxel we assume that all of the slices were acquired simultaneously. To make this assumption valid, the :ref:`time-series <Time_Series>` for each slice needs to be shifted back in time by the duration it took to acquire that slice. `Sladky et al. (2011) <https://www.sciencedirect.com/science/article/pii/S1053811911007245>`__ also demonstrated that slice-timing correction can lead to significant increases in statistical power for studies with longer TRs (e.g., 2s or longer), and especially in the dorsal regions of the brain.

Although slice-timing correction seems reasonable, there are some objections:

1. In general, it is best to not interpolate (i.e., edit) the data unless you need to;

2. For short TRs (e.g., around 1 second or less), slice-timing correction doesn't appear to lead to any significant gains in statistical power; and

3. Many of the problems addressed by slice-timing correction can be resolved by using a **temporal derivative** in the statistical model (discussed later in the chapter on model fitting).


We will do slice-timing correction to learn how to do it, using the first slice as the reference. But it will likely not to be needed for this dataset since the data were acquired using multiband technology (several slices are acquired at once) and the TR is rather short (1.5s)


Doing Slice-Timing Correction in SPM
***************************************

.. figure:: 01_slice_timing1.png

.. figure:: 01_slice_timing2.png
   :align: right

First click on the ``Slice Timing`` button in the SPM GUI. 


In this experiment, there were five runs of data per subject (SPM refers to each run as a **session**). If you click on the ``Data`` field, you will see an option to add more sessions. Click on ``New: Session`` to add another 4 sessions. You will see an ``<-X`` to the right of each Session field, indicating that this field needs to be filled in before the program can be run.

Double-click on the first session to open up the Image Selection window. Navigate to the ``func`` directory and select the file ``sub-[id]_task-faceobj_acq-mb3_run-01_bold.nii,1`` (replace [id] with the id of the subject you want process). The ``,1`` at the end of the file name indicates that only the first **frame**, or volume, is available for selection. In order to select all of the volumes for that run, we will need to **expand** the number of frames available for selection. In the ``Frames`` field (underneath the ``Filter`` field), type ``Inf`` and press enter.
Note that entering the string ``Inf`` in the ``Frames`` field will automatically expand to ``[1 2 3 4 5 ... 170]``. If for some reason ou want to select volumes 5 to 62, type ``5:62``

However, you will notice that all of the frames for all runs have been selected, even though we only want the frames for run-01. You could simply click and drag from frame 1 to frame 170 for run-01, but you risk accidentally including other frames by mistake. To restrict our file selection to only the frames we are interested in, on the other hand, we can use the ``Filter`` field. This field uses **regular expressions**, a type of coding shorthand to indicate which characters to include in a string. In this case, to the left of the ``.*`` characters that are already in the field, type ``run-01`` and press return. This will refresh the screen to display only those frames which include the string ``run-01``. Either click and drag to select all of the images, or right click in the selection window and click ``Select All``.

When you are finished, click ``Done``. Do the same procedure for the second session, using the ``Filter`` field to restrict your search to frames containing the string ``run-02``. Repeat for the remaining sessions.

For the ``Number of Slices`` field, we will need to find out how many slices there are in each of the volumes in our dataset. From the Matlab terminal navigate to the directory ``sub-[id]/func`` (replace [id] with the id of the subject you are processing) and type:

::

  V = spm_vol('sub-[id]_task-faceobj_run-01_bold.nii')
  
Replace [id] with the id of the subject you are processing. This will load the **header** of the image into a variable called ``V``. If you now type ``V`` and press return, you will see that it contains the following fields:

::

    fname
    dim
    dt
    pinfo
    mat
    n
    descrip
    private
    
``fname`` is the name of the file, and ``dim`` contains the dimensions for each volume in the file. (We won't be looking at the other fields right now; all you need to know is that they contain other header information that SPM needs to read the file.) If you type
 
::

  V(1).dim

It will return the dimensions of the first volume in the time-series in the x-, y-, and z-directions. You should see something like this:

::

  108 108 72
  
This means that the first volume of the time-series has the dimensions of 108x108x72 voxels, with 72 being the number of **slices** in the z-dimensions. We will assume that the dimensions of each image and the number of slices will be the same for every volume in the subject's functional data.

Now go back to the Batch Editor window, double-click on ``Number of Slices``, enter a value you got, and click ``OK``. 

You can also get this information by displaying the image using the ``Display`` button in SPM. It will show you the dimension on the image. The json file contains this information as well, in the field  "dcmmeta_shape": [108, 108, 72, 170]

For the TR, enter 1.5; for the TA, we are NOT going follow the formula provided in the help window and enter ``2-(2/72)``. Because our data were acquired several slices at a time (multiband), we will enter the acquisition time in ms. Then enter value ``0``.

.. note::
  TR versus TA
  Typically, we acquire fMRI data continuously with no gaps between volumes. Therefore, the acquisition time (TA) is directly related to the repeat time (TR). Specifically, the TA volume with N slices is TR-(TR/N). 
  If acquisition was set with a temporal gap between volumes (e.g. for sparse designs), the TA should account for it.

For Slice order, open the json file corresponding to your image and enter the 72 values contained in the field ``SliceTiming``. For the Reference Slice enter a value of ``0``. Leave the filename prefix as is, which will prepend an ``a`` to the files that are generated. Do this same procedure for run-02 to run-05. When you are finished, the preprocessing window should look like this:

.. figure:: 01_slice_timing3.png

Then save the batch. This is important to keep a trace of everything you run on the data. Click on the save icon, give you batch an explicit name, e.g. ``batch_sub-[id]_slice-timing.mat`` and place it in ``03_derivatives/spm-preproc/batches/``.

.. figure:: 01_save_batch.png

You can then run the job by clicking the green arrow.