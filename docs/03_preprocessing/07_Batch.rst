.. _07_Batch:

==================
Creating a batch 
==================


Why Making a Batch?
********************

Entering the information and data files for each step, each subject can be very time consuming. Using a batch that includes all the steps will make preparing the preprocessing much faster.

How to Make a Batch?
********************

SPM offers some batches that you can use. They are available in spm12/batches.

Click on ``Batch``  in the SPM menu, then from the batch window, click the icon to open files, navigate in  /usr/local/spm12/batches, and select ``preproc_fmri.m``

.. figure:: 07_spm_batch.png

In the bottom left window, in the Slice timing correction menu, select Do not perform STC

.. figure:: 07_noSTC.png

Then enter the number of runs when asked for Number of Sessions. A batch with all the preprocessing steps will be created. Note that only the first two steps contain ``<-X``. You will need to input some information there. The other step use dependencies from the output of previous steps.

.. figure:: 07_FullBatch.png