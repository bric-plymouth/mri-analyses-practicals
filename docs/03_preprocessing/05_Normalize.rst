.. _05_Normalize:

==============
Normalization
==============

Performing Normalization in SPM
*******************************

After the anatomical image has been segmented, we can use those segmentations to **normalize** the image. From the SPM GUI, click on ``Normalize (Write)``, click on the ``Data`` field in the Batch Editor, and create a new Subject. Select the Deformation Field that you created in the ``anat`` directory during Segmentation (the file should be called "y_rsub-[id]_T1w.nii"), and for ``Images to Write`` select all of the realigned (and unwarped) images. You can do this more efficiently by typing ``^r.*run-01.*`` (for realigned only) or ``^u.*run-01.*`` (for realigned & unwarped) in the Filter field, and entering ``Inf`` in the Frames field. 

.. Important::

  Make sure to include the functional images for all 5 runs!


In the ``Writing Options`` section, you can change the voxel resolution of the images that are warped. The default of 2x2x2 will create higher-resolution images, but the files will take up more space on your computer. You can create smaller files with lower resolution, e.g. ``3 3 3``. However it is best to use the actual resolution of your images. This can be found when displaying the image (vox size, ignore the minus sign), in the image header or in the json file.

.. note::

  You can also select the realigned anatomical image as an image to normalize, which may be useful if you want to visualize the individual subject's results on their own anatomy. For now, we will only be visualizing the results on a template brain, but we will see that in the batch section.


Checking the Output
*******************

Once the functional images have been normalized, check the output to make sure that there were no errors. From the SPM GUI, click on ``Check Reg``, and select one of your functional volumes that has a ``w`` prepended to it (indicating that it has been **warped** - that is, normalized). For the second image, go to the directory ``spm12/canonical`` and select any of the ``T1`` images - either avg152T1.nii, avg305T1.nii, or single_subj_T1.nii. As with :ref:`Coregistration <03_Coregistration>`, check to make sure that both the outlines of the brains and the internal structures are well-aligned.

.. note::

  The template ``single_subj_T1.nii`` will have the clearest spatial resolution - i.e., you will be able to see each of the individual gyri and sulci. However, visualizing your results on this template may be slightly misleading, since each subject's anatomy has been warped and blurred; activation that appears to be in a specific location on the single_subj_T1 template may not be as specific as it appears. For this reason, it is recommended to visualize your activation on one of the averaged templates, or on an average image consisting of the mean of your subject's normalized anatomical images. We will discuss this in more detail when we cover statistical modeling in the next chapter.
  
.. figure:: 05_CheckNormalization.gif

-----------------

Exercises
*********

1. Change the ``Voxel sizes`` from [2 2 2] to [3 3 3], also changing the Filename Prefix from ``w`` to ``w_3_3_3``. Compare the spatial resolution of the two outputs. What resolution seems best to you? What are the disadvantages of using a resolution that is very small, such as [1 1 1]?

2. As with coregistration, you can select different amount of Interpolation. The default is ``4th Degree B-Spline``, with options for higher degrees of interpolation (such as 7th Degree B-Spline), or lower degrees of interpolation - the lowest level being ``Nearest Neighbour``, which uses the value of the nearest voxel for resampling. When would a higher degree of interpolation be desirable, and when would you want to do Nearest Neighbour interpolation? Try using both interpolation options, and compare the results.
