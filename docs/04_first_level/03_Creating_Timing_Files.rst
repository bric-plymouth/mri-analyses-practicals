.. _05_Creating_Timing_Files:

================================
Creating Timing Files
================================

The Ideal Time-Series and the Fitted Time-Series
------------------------------------------------

We just saw how we can use several regressors, or independent variables, to estimate an outcome measure. Conceptually, we’re doing the same thing when we use several regressors to estimate brain activity, which is our outcome measure with fMRI data: We estimate the average amplitude of the BOLD signal in response to each condition in our model.

In the animation below, the different colors of the BOLD responses represent different conditions (such as seeing a face or an object), and the gray line represents the time-course of our preprocessed data. This shows how the amplitude of each condition is being estimated to best fit the data; for the condition on the left, it is relatively high, whereas for the condition on the right, it is relatively low. You can also imagine a condition's BOLD signal which is not significantly different from zero, or which is even negative.

.. figure:: 03_GLM_fMRI_Data.gif

The Red and Green lines representing the HRFs are called the **ideal time-series**. This is the time-series that we expect, given the timing of each stimulus in our experiment. When we estimate beta weights to fit this ideal time-series to the data, the result is called a **fitted time-series**, shown in the animation as a blue line. 

.. note::

  Since every voxel has its own time-series, we do the procedure above for every voxel in the brain. This is known as a **mass univariate** analysis, since we estimate beta weights for each voxel's time-series. As there are tens or hundreds of thousands of voxels in typical fMRI datasets, later we will need to correct for all of the tests we have done.


Creating the Ideal Time-Series
------------------------------

Our goal is to create the fitted time-series so that we can use the estimated beta weights in a group-level analysis. But to do that, we first need to create our ideal time-series.


In order to run the analysis, we need to tell SPM about all the source of variance that can explain the BOLD signal. This means that every stimulus and button press should be included in the model. 
For each event/condition, we need to specify:
1. The name of the condition (e.g. face or response);
2. When each event of that condition occurred, in seconds, relative to the start of the scan; and
3. The duration of each the event.

Exploring the logfiles
**********************

These need to be extracted from the logfiles produced by the stimulus presentation software and formatted in a way that the SPM software can read. We will create a timing file for each run. Each timing file will contain:

1. Timings for the Face trials;
2. Timings for the Object trials;
3. Timings for the Butterfly trials;
4. Timings for the Responses (button press) that occurred after seeing a butterfly.

.. admonition:: Exercise...

   Take a look at the logfiles. In the ``beh`` directory are files labeled ``[sub-id]_task-faceobj_run-[#].log``. 
   On the BRIC Azure Linux server, you can read those files using LibreOffice Calc (this is the equivalent of Excel on Windows).

.. figure:: 03_logfile.png

The logfile contains information about everything that has happened during the run, every stimulus presentation, button press and every (start of) acquisition of a new BOLD volume. This last piece of information is obtained by having the scanner send a 'pulse' signal to the stimulus computer at the start of each new image acquisition. 

There are 3 important columns that we need here, the ``Event Type``, ``Code`` and ``Time``. The ``Event Type`` column tells you whether the logged event was a change on the screen (``Picture``), a button press by the subject (``Response``), or the start of the acquisition of a new BOLD volume (``Pulse``). Each picture and button is associated with ``Code``. Subjects were always pressing the same button in the scanner, therefore we have one button code. For the pictures, we have:

- codes 0 = black screen
- codes 1-50 = faces
- codes 100-150 = objects
- codes 200+ = butterflies (target)

The ``Time`` column tells you when each event occurred. The time unit is tenth of milliseconds in presentation logfiles (this varies among software) so you'd need to divide the values by 10000 to get them in seconds.

.. Important::
   The time of acquisition of the first image of the run is your time 0 of the run. You need to remove the time of the first ``Pulse`` from all the event times.

.. admonition:: Exercise...

   check how many ``Pulse`` event there are in the logfile and how much time there is between two consecutive one. What do these numbers correspond to?

Extracting timing from the logfiles
***********************************

To tell SPM about the conditions and timing of our experiment, we need to retrieve three elements, in the following order:

1. Name of the event or condition (in our case we have 4: Face, Object, Butterfly, Response)
2. Onset time, in seconds, relative to the start of the scan; and
3. Duration of the trial, in seconds. 

.. admonition:: Exercise...

   Open SPM, click on ``Specify 1st level`` in the menu and explore the fields that need to be filled. Look at the explanations in the bottom box.

There are two ways to enter that information in SPM:

The first methods is to 

1. manually extract the times for the logfiles
2. create a new condition for each event type in SPM and input the time values.

.. Tip:: This methods is very tedious as you'll need to repeat the process 4(conditions) * 5(runs) * ~ 20-30 (participants, in a full study) times. Besides you are likely to make some errors and they will be difficult to spot. 

.. figure:: 03_creating_conditions.png

The second methods is to 

1. write a script that will store all the times for all the events/conditions in the single file per run. Each of these timing files (also known as **onset files**) will have the same format consisting of three elements mentioned above. 
2. Input this single file per run in SPM

.. Tip:: This methods is to be preferred because if you make a mistake, you can track it back in your script, fix it and run it again. It takes time to write a script the first time but in the long term, it is much more efficient.

.. figure:: 03_multiple_conditions.png

Creating the onset files with a script
**************************************

I have written a script to generate the onsets files. Let's have a quick look at what it does. 

**Exercise**

   1. In the matlab command window, type 
   ``addpath(genpath('/home/lab1user/SHARED_labdata/LAB-DATA/04_scripts/'))``.   
   This will temporarily allow matlab to access and run the scripts in that folder. 

   2. In matlab, type the following and replace the ``[sub-id]`` and ``[data_time_string]`` with the correct id and filename parts.
   ``EventLog = readlogs('home/lab1user/STUDENT-DATA/PSYC724-LAB-DATA/03_derivatives/spm-preproc/[sub-id]/beh/[sub-id]_task-faceobj_run-01_[data_time_string].log')``  
   This will create a variable called ``EventLog``. Double click on it and explore the table. Make sure the time of the first pulse is set to 0.

   3. Run create_onsets.m If you are curious about its content, it is located in 04_scripts/02_first_level, you can use the command 'open' in matlab. 
   This will create the onset files in ``03_derivatives/spm-stats/onsets/``. You can check the content of the files by using the button ``Import Data``. 


.. figure:: 03_onsets.png

Once you have created the timing files, you are now ready to use them to fit a model to the fMRI data. To see how to do that, click the Next button.

