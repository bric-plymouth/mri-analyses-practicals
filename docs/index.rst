.. fMRI analyses handbook created by
   sphinx-quickstart on Sat May 15 19:58:13 2021.

=======================================
MSc Human Neuroscience - MRI analyses
=======================================

This resource has been developed as a support the course on MRI Data Preprocessing and Analyses, module PSYC784 Advanced Practice in Neuroimaging and Neurostimulation at the University of Plymouth, UK.
The dataset has been acquired by the students enrolled in the MSc Human Neuroscience program. It is located on the BRIC data storage and analysis server (Azure plateform). Access to the server can be requested by University staff and student by contacting the BRIC system administrator, Paul Greening, or the BRIC lab heads. 

.. figure:: _static/FFA.gif
   :align: center

.. toctree::
   :caption: MRI analyses general
   :maxdepth: 2
   :hidden:

   01_general/general.rst
   01_general/resources.rst
   01_general/SPM12_overview.rst

.. MRI data overview

.. toctree::
   :caption: Data exploration and QA
   :maxdepth: 2
   :hidden:

   02_data_qa/01_task.rst
   02_data_qa/02_data.rst
   02_data_qa/03_LookingAtData.rst

.. Data exploration and QA

.. toctree::
   :caption: Preprocessing
   :maxdepth: 2
   :numbered:
   :hidden:

   03_preprocessing/00_PreprocessingOverview.rst
   03_preprocessing/01_SliceTiming.rst
   03_preprocessing/02_Realign_Unwarp.rst
   03_preprocessing/03_Coregistration.rst
   03_preprocessing/04_Segmentation.rst
   03_preprocessing/05_Normalize.rst
   03_preprocessing/06_Smoothing.rst
   03_preprocessing/07_Batch.rst
   03_preprocessing/08_Scripting.rst

.. SPM preprocessing

.. toctree::
   :caption: First level analyses
   :maxdepth: 2
   :numbered:
   :hidden:

   04_first_level/01_Stats_Overview.rst
   04_first_level/02_Stats_General_Linear_Model.rst
   04_first_level/03_Creating_Timing_Files.rst
   04_first_level/04_Stats_Running_1stLevel_Analysis.rst

.. First level analyses

.. toctree::
   :caption: Group level analyses
   :maxdepth: 1
   :hidden:

.. Glossary
.. ==================

.. * :ref:`terms`