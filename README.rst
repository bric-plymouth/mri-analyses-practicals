=======================================
MRI analyses handbook
=======================================

This resource has been developed as a support the course on MRI Data Preprocessing and Analyses, module PSYC724 Advanced Practice in Neuroimaging and Neurostimulation at the University of Plymouth, UK.
`Brain Research and Imaging Centre <https://www.plymouth.ac.uk/research/psychology/brain-research-and-imaging-centre>`_  of he university of Plymouth.

URL: https://mri-analyses-practicals.readthedocs.io/en/latest/

